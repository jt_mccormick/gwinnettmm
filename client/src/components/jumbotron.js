import React from 'react';
import {Jumbotron, Button} from 'react-bootstrap';

const MMJumbo=(props)=>(
    <Jumbotron>
        <h1>Gwinnett Market Models</h1>
  <p>
    Web application for auditing proposed market models for Gwinnett County
  </p>
    </Jumbotron>
)
export default MMJumbo;